# FDR

Some server daemons will offer serves to those that request it, such as NTP. Write a math server that will jprovide similar services to those that request a few specific items.

The server should accept UDP requests in one of three forms. All numeric responses should be in hexadecimal with a leading 0x.

Fnumber
Given a decimal number is between 0-300 (inclusive), the response packet should be F(number) in hexadecimal, where F() is the Fibonacci function.
Dnumber
Given a decimal number between 0-10^30 (inclusive), the response packet should be that number in hexadecimal.
Rnumber
Given a Roman numeral number between I-MMMM (inclusive), the response packet should be that number in hexadecimal.

The response packet should naturally be over UDP.

The request packets may have a trailing NUL, but no other bytes. All bytes in the request are ASCII. If the request in invalid (out of range, invalid characters, etc.), then it may be dropped with no notification.

The response must contain a trailing NUL.

## Sample Use

>% echo -n 'F10' | nc -q2 -u devprep 1002
>
>0x37
>
>% echo -n 'D10998723' | nc -q2 -u devprep 2002
>
>0xA7D3C3
>
>% echo -n 'RMDCDIIII' | nc -q2 -u devprep 1002
>
>0x770

## Requirements

The server should be invokable with no required arguments. It should listen on three ports: The invoking UID, UID+1000, UID+2000. The same services should be available on all three ports.
